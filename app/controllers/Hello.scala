package controllers

import akka.actor.{Actor, Props}
import akka.pattern.ask
import akka.util.Timeout
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.mvc.{Action, Controller}

import scala.concurrent.duration._

object Hello extends Controller {

  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  private val helloActor = Akka.system.actorOf(Props(new HelloActor()))
  case object Hello
  case class World(str: String)


  private class HelloActor extends Actor {
    def receive = result(1)

    def result(count: Int): Receive = {
      case Hello =>
        sender ! World(s"world $count")
        context.become(result(count + 1))
    }
  }

  def hello() = Action.async {
    implicit val timeout = Timeout(5.seconds)
    (helloActor ? Hello).mapTo[World].map(w => Ok(w.str))
  }
}
