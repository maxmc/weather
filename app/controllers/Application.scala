package controllers

import models.WeatherModule
import play.api.data.Form
import play.api.data.Forms.{mapping, text}
import play.api.i18n.Messages
import play.api.mvc.{Action, Controller, RequestHeader}

import scala.concurrent.Future

case class Search(city: String = "")

trait ApplicationCtrl extends Controller {
  this: WeatherModule =>

  implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

  def index = Action { implicit request =>
    Ok(views.html.index(form.fill(Search())))
  }

  def form(implicit request: RequestHeader) = Form(
    mapping("city" -> text.verifying(
      Messages("application.search.error.empty"),
      s => !s.isEmpty))(Search.apply)(Search.unapply))

  def submit = Action.async { implicit request =>
    form.bindFromRequest.fold(
      formWithErrors =>
        Future.successful {
          BadRequest(views.html.index(formWithErrors))
        },
      success =>
        weatherService.forecast(success.city).map { results =>
          Ok(views.html.index(form.fill(success), results))
        }
    )
  }
}