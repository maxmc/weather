package models

import java.util.Date
import play.api.Logger
import scala.concurrent.Future

case class Forecast(dt: Long, temp: Temp, weather: Seq[Weather]) {
  def date = new Date(dt * 1000) // dt appears to be in seconds, date expects ms
}

case class Temp(day: Float, min: Float, max: Float, night: Float)

case class Weather(description: String)

trait WeatherModule {
  type Forecasts = Seq[Forecast]

  def weatherService: WeatherService

  trait WeatherService {
    def forecast(city: String): Future[Forecasts]
  }
}

trait DefaultWeatherModule extends WeatherModule {

  def weatherService = new DefaultWeatherService

  class DefaultWeatherService extends WeatherService {

    import play.api.Play.current
    import play.api.cache.Cache
    import play.api.libs.json._
    import play.api.libs.ws.WS

    implicit val tempReads = Json.reads[Temp]
    implicit val weatherReads = Json.reads[Weather]
    implicit val forecastReads = Json.reads[Forecast]

    implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

    val forecastUrl = "http://api.openweathermap.org/data/2.5/forecast/daily"

    def forecast(city: String) =
      Cache.getAs[Forecasts](city).map { cached =>
        Future.successful(cached)
      }.getOrElse {
        fetch(city).map { forecasts =>
          Cache.set(key = city, value = forecasts, expiration = 10)
          forecasts
        }
      }

    def fetch(city: String): Future[Forecasts] =
      WS.url(forecastUrl).withQueryString(
        "q" -> city,
        "mode" -> "json",
        "units" -> "metric").get().map { result =>
        transform(result.json)
      }.recover {
        case e: Exception =>
          Logger.error(s"An error while calling the webservice...${e.getMessage}")
          Seq()
      }

    def transform(json: JsValue): Forecasts =
      (json \ "list").validate[Forecasts] match {
        case s: JsSuccess[Forecasts] =>
          s.get
        case e: JsError =>
          Logger.warn(JsError.toFlatJson(e).toString())
          Seq()
      }
  }

}