import models.{DefaultWeatherModule, Forecast}
import org.junit.runner.RunWith
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatestplus.play._
import org.specs2.runner.JUnitRunner

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class WeatherModuleSpec extends PlaySpec with OneAppPerSuite with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(3, Seconds), interval = Span(5, Millis))

  class DefaultWeatherModuleTest extends DefaultWeatherModule

  def service = new DefaultWeatherModuleTest().weatherService

  "WeatherModule" must {

    "return the weather forecast if the input is valid" in {
      val result = service.forecast("Cologne")
      result.futureValue.size must equal(7)
    }

    "return an empty seq in case the city is unknown" in {
      val result = service.forecast("")
      result.futureValue.size must equal(0)
    }
  }
}
