import controllers.ApplicationCtrl
import models.WeatherModule
import org.junit.runner.RunWith
import org.scalatestplus.play._
import org.specs2.runner.JUnitRunner
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future

@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends PlaySpec with OneAppPerSuite {

  implicit override lazy val app = FakeApplication()

  trait TestWeatherModule extends WeatherModule {
    def weatherService = new TestWeatherService

    class TestWeatherService extends WeatherService {
      def forecast(city: String) = Future.successful(Seq())
    }
  }

  class TestCtrl extends ApplicationCtrl with TestWeatherModule

  "ApplicationCtrl" must {

    "serve index" in {
      val result = new TestCtrl().index().apply(FakeRequest())
      status(result) mustEqual OK
    }

    "accept valid submissions" in {
      val result = new TestCtrl().submit().apply(
        FakeRequest().withFormUrlEncodedBody(
          "city" -> "Cologne"))
      status(result) mustEqual OK
    }

    "fail on an empty submission" in {
      val result = new TestCtrl().submit().apply(
        FakeRequest().withFormUrlEncodedBody(
          "city" -> ""))
      status(result) mustEqual BAD_REQUEST
    }
  }
}
